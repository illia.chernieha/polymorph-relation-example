<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Log;

class TestMorphController extends Controller
{

    public function getImagesWithRelationPolymorph()
    {
        Log::info('IMAGES WITH POLYMORPH RELATION');
        $users = User::find(1)->with('posts');
        $user = $users->get()->first();

        $posts = $user->posts;
        $postWithImage = [];
        foreach ($posts as $post) {
            $postWithImage[$post['id']] = $post->image;
        }
        $usersImage = $user->image;

        return response()->json([
            'user' => $user,
            'image' => $usersImage,
            'users_post' => [
                'posts' => $posts,
                'posts_image' => $postWithImage
            ]
        ]);
    }

    public function getImagesWithRelation()
    {
        Log::info('IMAGES WITH ONE TO MANY RELATION');
        $user = User::find(1);
        $userWithNews = User::find(1)->news;
        $userWithPublications = User::find(1)->publications;

        return response()->json([
            'user' => $user,
            'publications' => $userWithPublications,
            'news' => $userWithNews
        ]);
    }
}
