<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;


    protected $table = 'users';
    protected $fillable = [
        'name',
    ];

    public function posts()
    {
        return $this->hasMany(Posts::class, 'user_id', 'id');
    }

    public function image()
    {
        return $this->morphOne(Images::class, 'imageable');
    }

    public function news()
    {
        return $this->hasMany(News::class, 'user_id', 'id');
    }

    public function publications()
    {
        return $this->hasMany(Publications::class, 'user_id', 'id');
    }
}
