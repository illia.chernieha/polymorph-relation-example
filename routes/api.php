<?php

use App\Http\Controllers\TestMorphController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('get-relations-polymorph', [TestMorphController::class, 'getImagesWithRelationPolymorph']);
Route::get('get-relations', [TestMorphController::class, 'getImagesWithRelation']);
